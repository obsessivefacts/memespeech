'use strict';

const DEBUG_MODE = true;

self.addEventListener('message', function(e) {
    switch (e.data.call) {
        case 'sayHello':
            debugWrite('Worker is active! Hello :)');
            break;
        case 'analyzeCarrierText':
            analyzeCarrierText(e.data.text);
            break;
        case 'toString':
            toString(
                e.data.cryptographicPayload,
                e.data.carrierText,
                e.data.matches
            );
            break;
        case 'parse':
            parse(e.data.matches);
            break;
        case 'computeCryptographicPayload':
            computeCryptographicPayload(
                e.data.ciphertext,
                e.data.carrierTextBudget,
                e.data.pbkdf2Salt,
                e.data.pbkdf2Iterations,
                e.data.encodingMode
            )
            break;
    }
    }, false);

let debugWrite = function(message) {
    if (DEBUG_MODE) self.postMessage({call: 'debugWrite', message: message})
}

let analyzeCarrierText = function(text) {
    try {
        let matches = [...text.matchAll(/[A-Za-z]/g)];
        self.postMessage({
            call: 'analyzeCarrierText:resolve',
            matches: matches
        });
    } catch(err) {
        self.postMessage({
            call: 'analyzeCarrierText:reject',
            err: err.toString()
        });
    }
}

let computeCryptographicPayload = function(ciphertext, bitBudget, salt, iter,
    encodingMode) {
    try {
        let id = '(Memespeech.computeCryptographicPayload)',
            ciphertextBitSize = ciphertext.length * 8;

        if (!ciphertext)
            return self.postMessage({
                call: 'computeCryptographicPayload:reject',
                err: id + ': ciphertext is missing. Please encrypt first!'
            })

        let footerPacketBits = encodingMode == 'standard' ? 262 : 69,
            ciphertextPacketBudget = bitBudget - footerPacketBits,
            minCiphertextChunks = (Math.floor(ciphertextBitSize / 255) + 1),
            minCiphertextPacketSize = 9*minCiphertextChunks + ciphertextBitSize;

        if (minCiphertextPacketSize > ciphertextPacketBudget) {
            return self.postMessage({
                call: 'computeCryptographicPayload:reject',
                err: id +': Cryptographic Payload cannot fit within the Carrier'
                        + ' Text. Please try again with larger Carrier Text or'
                        + ' smaller ciphertext.'
            })
        }

        let ciphertextBits = Memespeech.DataUtils.bytesToBits(ciphertext),
            budgetUsage = minCiphertextPacketSize,
            chunks = [],
            anchor = 0;

        for (var i=0; i<minCiphertextChunks-1; i++) {
            if (budgetUsage + 9 < ciphertextPacketBudget) {
                let slicey = Math.floor(Math.random() * 255) + 1;
                chunks.push(ciphertextBits.slice(anchor, anchor + slicey));
                chunks.push(ciphertextBits.slice(anchor + slicey, anchor + 255))
                budgetUsage += 9;
            } else {
                chunks.push(ciphertextBits.slice(anchor, anchor + 255));
            }
            anchor += 255;
        }
        if (chunks.length == 0 && budgetUsage + 9 <= ciphertextPacketBudget) {
            let slicePoint = Math.floor(Math.random()*ciphertextBits.length)+1;
            chunks.push(ciphertextBits.slice(0, slicePoint));
            chunks.push(ciphertextBits.slice(slicePoint, 255));
        } else {
            chunks.push(ciphertextBits.slice(anchor, anchor + 255));
        }

        let cryptographicPayload = [];

        for (let i=0; i<chunks.length; i++) {
            let lengthByte = Memespeech.DataUtils.intToBits(chunks[i].length);
            for (let bit of lengthByte) cryptographicPayload.push(bit);
            for (let bit of chunks[i]) cryptographicPayload.push(bit);
            cryptographicPayload.push(i == chunks.length - 1 ? 1 : 0);
        }

        let saltBits = Memespeech.DataUtils.bytesToBits(salt),
            iterationsBits = Memespeech.DataUtils.byteToBits(iter / 10000, 4);
        
        if (encodingMode == 'standard') {
            cryptographicPayload.push(0);
            cryptographicPayload.push(1);
        } else {
            cryptographicPayload.push(1);
        }
        for (let bit of saltBits) cryptographicPayload.push(bit);
        for (let bit of iterationsBits) cryptographicPayload.push(bit);

        self.postMessage({
            call: 'computeCryptographicPayload:resolve',
            payload: cryptographicPayload
        });
    } catch(err) {
        self.postMessage({
            call: 'computeCryptographicPayload:reject',
            err: err.toString()
        });
    }
}

let toString = function(cryptographicPayload, carrierText, matches) {
    let id = '(Memespeech.toString)'

    if (!carrierText) {
        self.postMessage({
            call: 'toString:reject',
            err: id +': No Carrier Text found. Start over or RTFM or something.'
        });
    }

    try {
        let carrierPos = 0,
            ciphertextPos = 0,
            memespeechStr = '';

        for (var i=0; i<matches.length; i++) {
            let match = matches[i],
                matchPos = match.index;

            for (; carrierPos <= matchPos; carrierPos++) {
                if (carrierPos < matchPos) {
                    memespeechStr += carrierText[carrierPos];
                } else {
                    let matchedChar = carrierText[carrierPos];
                    
                    if (ciphertextPos < cryptographicPayload.length) {
                        if (cryptographicPayload[ciphertextPos] == 0) {
                            memespeechStr += matchedChar.toLowerCase();
                        } else {
                            memespeechStr += matchedChar.toUpperCase();
                        }
                        ciphertextPos++
                    } else {
                        if (Math.random() < .5) {
                            memespeechStr += matchedChar.toLowerCase();
                        } else {
                            memespeechStr += matchedChar.toUpperCase();
                        }
                    }
                }
            }
        }
        for (; carrierPos < carrierText.length; carrierPos++) {
            memespeechStr += carrierText[carrierPos];
        }
        self.postMessage({
            call: 'toString:resolve',
            memespeechStr: memespeechStr
        });
    } catch(err) {
        self.postMessage({
            call: 'toString:reject',
            err: err.toString()
        });
    }
}

let parse = function(matches) {
    let id = '(Memespeech.parse)';

    try {
        let rawBinary = [],
            ciphertextBits = [],
            cursor = 0,
            doneParsingCiphertextPacket = false;

        for (let match of matches) {
            rawBinary.push(match[0].toLowerCase() === match[0] ? 0 : 1);
        }

        let _incrementCursor = function(inc) {
            cursor += inc;
            if (cursor >= rawBinary.length) {
                return self.postMessage({
                    call: 'parse:reject',
                    err: id +': Unable to parse Memespeech Text. Bad data?? :('
                })
            }
        }

        while (doneParsingCiphertextPacket == false) {
            _incrementCursor(8);
            
            let lengthBits = rawBinary.slice(cursor - 8, cursor),
                length = Memespeech.DataUtils.bitsToByte(lengthBits);

            _incrementCursor(length);

            let ciphertextSliceBits = rawBinary.slice(cursor - length, cursor);
            for (let bit of ciphertextSliceBits) ciphertextBits.push(bit);

            if (rawBinary[cursor] === 1) {
                doneParsingCiphertextPacket = true;
            }
            _incrementCursor(1);
        }

        if (rawBinary[cursor] == 1) {
            var saltLength = 64,
                encodingMode = 'compact';
            _incrementCursor(1);
        } else if (rawBinary[cursor + 1] == 1) {
            var saltLength = 256,
                encodingMode = 'standard';                
            _incrementCursor(2);
        } else {
            return self.postMessage({
                call: 'parse:reject',
                err: id +': Cannot parse non-default Ciphertext Payload '
                        +'Footer Packet.'
            })
        }

        _incrementCursor(saltLength);
        let saltBits = rawBinary.slice(cursor - saltLength, cursor);

        _incrementCursor(4);
        let iterationsBits = rawBinary.slice(cursor - 4, cursor);

        self.postMessage({
            call: 'parse:resolve',
            ciphertext: Memespeech.DataUtils.bitsToBytes(ciphertextBits),
            encodingMode: encodingMode,
            pbkdf2Salt: Memespeech.DataUtils.bitsToBytes(saltBits),
            pbkdf2Iterations: Memespeech.DataUtils.bitsToInt(iterationsBits)*10000
        });
    } catch(err) {
        self.postMessage({
            call: 'parse:reject',
            err: err.toString()
        });
    }
}

let Memespeech = {};

Memespeech.DataUtils = {
    bytesToBits: function(uint8Array, debugMode) {
        let allBits = [];

        for (let byte of uint8Array) {
            for (let bit of this.byteToBits(byte, debugMode)) {
                allBits.push(bit);
            }
        }
        return allBits;
    },

    byteToBits: function(int, length) {
        length || (length = 8)
        let bits = new Array(length);
        for (var i = length - 1; i >= 0; i--) {
            bits[length - 1 - i] = (int >> i) & 1;
        }

        return bits;
    },

    bitsToByte: function(bits) {
        let int = 0;
        for (var i = 7; i >= 0; i--) { int = (bits[7 - i] << i) | int; }

        return int;
    },

    bitsToBytes: function(bits) {
        let bytes = [];

        for (let i=0; i<bits.length; i+=8) {
            bytes.push(this.bitsToByte(bits.slice(i, i+8)));
        }
        return Uint8Array.from(bytes);
    },

    intToBits: function(int, debugMode) {
        let binaryStr = int.toString(2),
            binary = [];

        while (binaryStr.length % 8 !== 0) {
            binaryStr = '0' + binaryStr;
        }
        for (var i=0; i<binaryStr.length; i++) {
            binary.push(parseInt(binaryStr[i]));
        }
        return binary;
    },

    bitsToInt: function(bits) {
        return parseInt(bits.join(''), 2);
    }
};